﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demoweb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("fileupload")]

        public ActionResult FileUpLoad(HttpPostedFileBase file)
        {
            if (file != null)
            {
                //5M
                if (file.ContentLength < 5 * 1024 * 1024)
                {
                    //获取文件类型
                    string fileType = System.IO.Path.GetExtension(file.FileName);
                    //创建文件名称
                    string fileName = Guid.NewGuid().ToString()+fileType;
                    string dir = Server.MapPath("~/Document/UploadFile");
                    if (!System.IO.Directory.Exists(dir))
                    {
                        System.IO.Directory.CreateDirectory(dir);
                    }
                    //保存文件的完整陆军
                    string fullPath = System.IO.Path.Combine(dir,fileName);
                    if (!string.IsNullOrEmpty(fileType))
                    {
                        file.SaveAs(fullPath);
                        return Json(new { success = true, msg = "文件上传成功" },"application/json");
                    }
                }
            }
            return Json(new { success = false, msg = "文件上传失败" },"application/json");
        }


        public FileStreamResult fileload()
        {
            string path = Server.MapPath("~/1.jpg");
            FileStream stream = new FileStream(path, FileMode.Open);
            return File(stream,"text/plain", "1.jpg");
        }
    }
}