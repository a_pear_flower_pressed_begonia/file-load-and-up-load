﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demowebcore.Models
{
    public class FileUpload
    {
        public List<IFormFile> FormFiles { get; set; }
    }
}
