﻿using demowebcore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace demowebcore.Controllers
{
    public class FileController : Controller
    {
        // GET: FileController
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> FileUpLoad(List<IFormFile> files)
        {
            if (files != null && files.Count > 0)
            {
                foreach (var item in files)
                {
                    string fullpath = System.IO.Path.GetFullPath("./");
                    string dir = System.IO.Path.Combine(fullpath, "FileUpLoad");
                    if (!System.IO.Directory.Exists(dir))
                    {
                        System.IO.Directory.CreateDirectory(dir);
                    }
                    string filename = item.FileName;
                    string filePath = System.IO.Path.Combine(dir, filename);
                    using (var stream = System.IO.File.Create(filePath))
                    {
                        await item.CopyToAsync(stream);
                    }
                }
                return Ok(new { success=true, msg="上传成功"});
            }
            return Ok(new { success=false, msg="上传失败"});
        }
    }
}
