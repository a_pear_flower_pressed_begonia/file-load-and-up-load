﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test();
            Console.ReadKey();
        }

        private static void Test()
        {
            Console.WriteLine((0x00)-1);// 0 - 1 --> -1
            Console.WriteLine(0x00);//16^0 * 0-->0
            Console.WriteLine(0x01);//16^0 * 1-->1
            Console.WriteLine(0x02);//16^0 * 2-->2
            Console.WriteLine(0x03);//16^0 * 3-->3
            Console.WriteLine(0x04);//16^0 * 4-->4
            Console.WriteLine(0x11);//16^1 * 1 + 16^0 * 1-->17
            Console.WriteLine(0x000111);//16^2 * 1 + 16^1 * 1 + 16^0 * 1--->273
        }
    }
}
