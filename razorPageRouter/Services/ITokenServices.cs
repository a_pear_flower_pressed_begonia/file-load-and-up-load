﻿using razorPageRouter.Models;
using razorPageRouter.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Services
{
    public interface ITokenServices : IServices
    {
        bool AddTokenTodb(Token.Token token);
        AuthorToken_App IsValidToken();

        bool IsValidToken(string token);
    }

    public class TokenServices : BaseServices, ITokenServices
    {
        protected ITokenRepository _tokenRepository;
        public TokenServices(ITokenRepository tokenRepository)
        {
            _tokenRepository = tokenRepository;
        }

        public bool AddTokenTodb(Token.Token token)
        {
            bool success = false;
            if(token != null)
            {
                try
                {
                    success = _tokenRepository.AddTokenToDb(token);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return success;
        }

        public AuthorToken_App IsValidToken()
        {
            AuthorToken_App mo = null;
            try
            {
                mo = _tokenRepository.IsValidToken();
                if (mo != null)
                {
                    if (mo.ctime.AddMinutes(mo.effect) > DateTime.Now)
                    {
                        return mo;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mo;
        }

        /// <summary>
        /// 验证token是否失效
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool IsValidToken(string token)
        {
            bool success = false;
            try
            {
                success = _tokenRepository.IsValidToken(token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return success;
        }
    }
}
