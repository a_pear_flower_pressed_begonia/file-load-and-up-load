﻿using razorPageRouter.Pages;
using razorPageRouter.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Services
{
    
    public interface IUserServices
    {
        bool ValidateAccout(User user);
    }

    public class UserServices : IUserServices
    {
        protected IUserRepository _userRepository;
        public UserServices(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool ValidateAccout(User user)
        {
            bool success = false;
            try
            {
                success = _userRepository.ValidateAccout(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return success;
        }
    }
}
