using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using razorPageRouter.Models;

namespace razorPageRouter.Pages
{
    [BindProperties]
    [Bind]
    public class fileuploadModel : PageModel
    {
        [BindProperty]
        [ModelBinder]
        public FileUpLoadM FileUpLoad { get; set; }

        [BindProperty]
        [ModelBinder]
        public IFormFile file { get; set; }

        public string Result { get; set; }

        public fileuploadModel()
        {
            FileUpLoad = new FileUpLoadM();
        }

        public void OnGet()
        {
            
        }

        /// <summary>
        /// 文件上传后台处理 接收上传的文件
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostUploadAsync()
        {
            //判断模型校验是否通过
            if (!ModelState.IsValid)
            {
                Result = "请选择要上传的文件";
                return Page();
            }
            foreach (var item in this.FileUpLoad.FileUpLoad)
            {
                string dir = System.IO.Directory.GetCurrentDirectory();
                dir = System.IO.Path.Combine(dir,"uploadfile");
                if (!System.IO.Directory.Exists(dir))
                {
                    System.IO.Directory.CreateDirectory(dir);
                }
                string filename = item.FileName;
                string filepath = System.IO.Path.Combine(dir,filename);
                using (var stream = System.IO.File.Create(filepath))
                {
                    await item.CopyToAsync(stream);
                }
            }
            /*return new JsonResult(new { success=true,msg= "上传成功"});*/
            return new OkObjectResult(new { success = true , msg="成功"});
        }
    }
}
