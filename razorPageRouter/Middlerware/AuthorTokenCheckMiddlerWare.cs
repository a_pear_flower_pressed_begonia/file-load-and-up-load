﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using razorPageRouter.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Middlerware
{
    public class AuthorTokenCheckMiddlerWare
    {
        private readonly RequestDelegate _next;
        public AuthorTokenCheckMiddlerWare(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            //验证token
            //获取token
            var token = httpContext.Request.Headers["token"];
            var path = httpContext.Request.Path;
            Console.WriteLine($"path={path}");
            if (!(path.Equals("/Login/Index") || path.Equals("/api/Author/validatetoken")))
            {
                if (string.IsNullOrEmpty(token.ToString()))
                {
                    await TokenValidate(httpContext);
                }
                else
                {
                    Console.WriteLine($"---->token={token}");
                    //验证token是否是签发的token
                    await _next.Invoke(httpContext);
                }
            }
            else
            {
                await _next.Invoke(httpContext);
            }
        }

        public async Task TokenValidate(HttpContext httpContext)
        {
            IResponses responses = new BaseResponses();
            responses.message = "请先进行登录验证获取token信息";
            var result = JsonConvert.SerializeObject(responses);
            await httpContext.Response.WriteAsync(result, System.Text.Encoding.UTF8);
        }
    }
}
