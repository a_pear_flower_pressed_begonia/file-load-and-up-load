﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Models
{
    public class AuthorToken_App
    {
        public int id { get; set; }
        public string token { get; set; }
        public int effect { get; set; }

        public DateTime ctime { get; set; }
        public string requestip { get; set; }
        public int requestct { get; set; }
    }
}
