﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Models
{
    public class FileUpLoadM
    {
        [Required]
        public List<IFormFile> FileUpLoad { get; set; }
    }
}
