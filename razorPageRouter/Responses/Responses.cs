﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Responses
{
    public class BaseResponses : IResponses
    {
        public int code { get; set; } = (0x00)-1;
        public bool success { get; set; } = false;
        public object data { get; set; } = null;
        public string message { get; set; } = "";
        public string error { get; set; } = "";
    }

    public class BaseResponses<T> : IResponses<T>
    {
        public int code { get; set; } = (0x00)-1;
        public bool success { get; set; } = false;
        public T data { get; set; } = default(T);
        public List<T> datas { get; set; } = null;
        public string error { get; set; } = "";
        public string message { get; set; } = "";
    }

    
}
