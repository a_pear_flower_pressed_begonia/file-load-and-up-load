﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Responses
{
    interface IResponses
    {
        public int code { get; set; }
        public bool success { get; set; }
        public object data { get; set; }
        public string message { get; set; }
        public string error { get; set; }
    }

    interface IResponses<T>
    {
        public int code { get; set; }
        public bool success { get; set; }
        public T data { get; set; }
        public List<T> datas { get; set; }
        public string error { get; set; }
        public string message { get; set; }
    }
}
