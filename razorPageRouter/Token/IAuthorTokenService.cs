﻿using Microsoft.AspNetCore.Http;
using razorPageRouter.Db;
using razorPageRouter.Models;
using razorPageRouter.Pages;
using razorPageRouter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace razorPageRouter.Token
{
    public interface IAuthorTokenService
    {
        IHttpContextAccessor _httpContextAccessor { get; set; }
        IUserServices _userServices { get; set; }
        ITokenServices _tokenServices { get; set; }
        Token _token { get; set; }
        Token CreateToken();

        bool ValidateToken(Token token);
        bool ValidateLogin(User user);

        string GetNewToken();
    }

    public class AuthorTokenService : IAuthorTokenService
    {
        public Token _token { get; set; }
        public IHttpContextAccessor _httpContextAccessor { get; set; }
        public IUserServices _userServices { get; set; }
        public ITokenServices _tokenServices { get; set; }

        public AuthorTokenService(
            IHttpContextAccessor httpContextAccessor,
            IUserServices userServices,
            ITokenServices tokenServices
            )
        {
            _httpContextAccessor = httpContextAccessor;
            _userServices = userServices;
            _tokenServices = tokenServices;
        }

        public Token CreateToken()
        {
            Token token = new Token();

            //判断现有token是否失效
            AuthorToken_App isInvalid = _tokenServices.IsValidToken();
            if(isInvalid != null)
            {
                token.token = isInvalid.token;
                token.EffectTime = isInvalid.effect;
                token.Ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                token.StartTime = isInvalid.ctime;
                return token;
            }
            token.token = GetNewToken();// Guid.NewGuid().ToString().Replace("-", "");//生成token
            token.EffectTime = 120;
            token.StartTime = DateTime.Now;
            token.Ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            _token = token;
            //保存token
            bool result = _tokenServices.AddTokenTodb(token);
            return result ? token : null;
        }

        public bool ValidateToken(Token token)
        {
            bool success = false;
            if (_token.token.Equals(token.token))
            {
                success = true;
            }
            return success;
        }

        public bool ValidateLogin(User user)
        {
            return _userServices.ValidateAccout(user);
        }

        public string GetNewToken()
        {
            Random random = new Random();
            StringBuilder token = new StringBuilder();
            string source_num = "1234567890";
            string source_abc = "abcdefghijklmnopqestuvwxyz";
            string source_ABC = "ABCDEFGHIJKLMNOPQESTUVWXYZ";
            string[] sources = new string[] { source_num, source_abc, source_ABC };
            for (int i = 0; i < 128; i++)
            {
                int group = random.Next(0,3);
                switch (group)
                {
                    case 0:
                        token.Append(source_num[random.Next(0,source_num.Length)]);
                        break;
                    case 1:
                        token.Append(source_abc[random.Next(0, source_abc.Length)]);
                        break;
                    case 2:
                        token.Append(source_ABC[random.Next(0, source_ABC.Length)]);
                        break;
                    default:
                        break;
                }
            }
            return token.ToString();
        }

    }

    public class Token
    {
        public string token { get; set; }
        public int EffectTime { get; set; }
        public DateTime StartTime { get; set; }
        public string Ip { get; set; }
        public int ErrorCount { get; set; }
    }

}
