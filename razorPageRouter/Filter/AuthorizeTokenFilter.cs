﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using razorPageRouter.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Filter
{
    public class AuthorizeTokenFilter : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var path = context.HttpContext.Request.Path;
            Console.WriteLine("Request--->Path--->"+path);
            var token = context.HttpContext.Request.Headers["token"];
            Console.WriteLine("Request--->token--->"+token);
            if (string.IsNullOrEmpty(token))
            {
                IResponses responses = new BaseResponses();
                responses.message = "token is invalid. please try again token";
                var result = JsonConvert.SerializeObject(responses,Formatting.Indented);
                Console.WriteLine(result);
                context.Result = new JsonResult(responses);
            }
        }
    }
}
