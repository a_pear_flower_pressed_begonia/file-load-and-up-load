﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using razorPageRouter.Enum;
using razorPageRouter.JiaMi;
using razorPageRouter.Pages;
using razorPageRouter.Responses;
using razorPageRouter.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorTokenService _authorTokenService;
        public AuthorController(IAuthorTokenService authorTokenService)
        {
            _authorTokenService = authorTokenService;
        }

        [HttpPost]
        [Route("validatetoken")]
        public IActionResult ValidateToken(User user)
        {
            Token.Token token = null;
            IResponses responses = new BaseResponses();
            if (user != null)
            {
                //user.UserName.Equals("admin") && user.PassWord.Equals(Encrytion.Md5("123456"))
                if (_authorTokenService.ValidateLogin(user))
                {
                    token = _authorTokenService.CreateToken();
                }
            }
            if (token != null)
            {
                responses.code = (int)ResponsesType.OK;
                responses.data = token;
                responses.success = true;
                responses.message = "获取token成功";
            }
            else
            {
                responses.message = "验证失败,签发token失败";
            }
            return Ok(responses);
        }

        [HttpPost]
        [Route("recvtoken")]
        public IActionResult RecvToken(Token.Token token)
        {
            return Ok(token);
        }
    }
}
