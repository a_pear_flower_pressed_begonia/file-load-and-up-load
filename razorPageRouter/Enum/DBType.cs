﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Enum
{
    public enum DBType
    {
        MYSQL,
        ORACLE,
        SQLSERVER
    }
}
