﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Enum
{
    public enum ResponsesType
    {
        DEFAULT = (0x00)-1,
        OK = 0x00,
        ERROR = 0x01,
        FAILED = 0x02,
    }
}
