using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using razorPageRouter.Db;
using razorPageRouter.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace razorPageRouter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //解决json序列化 前端显示中文的问题
            services.AddControllers().AddJsonOptions(options=> {
                options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
            });

            services.AddRazorPages();
            services.AddMvc();
            services.AddSingleton<string>(Configuration.GetSection("ConnectionStrings").GetSection("mysql").Value);
            //services.Configure<string>("constr", 
            //    Configuration.GetSection("ConnectionStrings").GetSection("mysql"));
            services.AddSingleton<IDbHelper, DbHelper>();

            //注入自定义服务
            services.AddServices();

            //注入token服务
            services.AddAuthorToken();

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            //注册自定义token验证中间件
            //app.AddAuthorTokenMiddleWare();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute(
                    name:"ysroute",
                    pattern:"{controller}/{action}/{id?}",
                    defaults:new { controller="Home", action="Index"}
                );

                //当有属性路由时 默认路由无效 属性路由会覆盖掉默认路由
                endpoints.MapControllerRoute(
                    name:"ysApi",
                    pattern:"api/{controller}/{action}"
                );
            });
        }
    }
}
