﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace razorPageRouter.JiaMi
{
    public class Encrytion
    {
        /// <summary>
        /// 输出32位的MD5大写加密字符串
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        public static string Md5(string enCode)
        {
            string result = "";
            using (MD5 md5 = MD5.Create())
            {
                byte[] encods = md5.ComputeHash(Encoding.UTF8.GetBytes(enCode));
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < encods.Length; i++)
                {
                    sb.Append(encods[i].ToString("x2"));
                }
                result = sb.ToString().ToUpper();
            }
            return result;
        }
    }
}
