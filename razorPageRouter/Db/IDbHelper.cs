﻿using razorPageRouter.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;

namespace razorPageRouter.Db
{
    public delegate IDbConnection DbCall(string constr);
    public class Const
    {
        public static Dictionary<DBType, DbCall> dbcalls =
            new Dictionary<DBType, DbCall>() { 
                { DBType.MYSQL, CreateMySqlCall},
                { DBType.ORACLE, CreateOracleCall},
                { DBType.SQLSERVER, CreateSqlServerCall},
            };

        public static IDbConnection CreateMySqlCall(string constr)
        {
            IDbConnection dbConnection = null;
            dbConnection = new MySqlConnection(constr);
            return dbConnection;
        }
        public static IDbConnection CreateOracleCall(string constr)
        {
            IDbConnection dbConnection = null;
            dbConnection = new OracleConnection(constr);
            return dbConnection;
        }

        public static IDbConnection CreateSqlServerCall(string constr)
        {
            IDbConnection dbConnection = null;
            dbConnection = new SqlConnection(constr);
            return dbConnection;
        }
    }
    public interface IDbHelper
    {
        string Constr { get; set; }
        DBType dBType { get; set; }
        IDbConnection dbConnection { get; set; }
        IDbConnection GetDbConnection();
        IDbConnection GetDbConnection(string constr,DBType dBType);
    }

    public class DbHelper : IDbHelper
    {
        public DbHelper(string constr)
        {
            this.Constr = constr;
        }

        public string Constr { get; set; }
        public DBType dBType { get; set; }
        public IDbConnection dbConnection { get; set; }

        public IDbConnection GetDbConnection()
        {
            return Const.dbcalls[dBType].Invoke(Constr);
        }

        public IDbConnection GetDbConnection(string constr, DBType dbType)
        {
            return Const.dbcalls[dbType].Invoke(constr);
        }
    }
}
