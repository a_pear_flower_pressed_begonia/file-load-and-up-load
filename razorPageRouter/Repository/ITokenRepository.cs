﻿using razorPageRouter.Db;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using razorPageRouter.Models;

namespace razorPageRouter.Repository
{
    public interface ITokenRepository : IRepository
    {
        bool AddTokenToDb(Token.Token token);
        bool IsValidToken(string token);
        AuthorToken_App IsValidToken();
    }

    public class TokenRepository:BaseRepository,ITokenRepository
    {
        public TokenRepository(IDbHelper dbHelper):base(dbHelper)
        {
            dbHelper.dBType = Enum.DBType.MYSQL;
        }

        public bool AddTokenToDb(Token.Token token)
        {
            bool success = false;
            string exissql = "select count(1) from AuthorToken_App ";
            string updsql = "update AuthorToken_App set token = @token , effect= @effect, requestip = @requestip , ctime = @ctime order by ctime desc limit 1 ";
            string sql = "insert into AuthorToken_App(token,effect,requestip,requestct) " +
                "values(@token, @effect, @requestip, @requestct) ";
            using (IDbConnection dbConnection = dbHelper.GetDbConnection())
            {
                var res = dbConnection.ExecuteScalar<int>(exissql);
                int ret = 0;
                //不存在就保存
                if (res <= 0)
                {
                    ret = dbConnection.Execute(sql,
                        new
                        {
                            token = token.token,
                            effect = token.EffectTime,
                            requestip = token.Ip,
                            requestct = 1
                        }
                    );
                    if (ret > 0)
                    {
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }
                }
                else
                {
                    //存在就更新token
                    ret = dbConnection.Execute(updsql, new { 
                        token = token.token,
                        effect = token.EffectTime,
                        ctime = DateTime.Now,
                        requestip = token.Ip
                    });
                    success = ret > 0 ? true : false;
                }
            }
            return success;
        }

        /// <summary>
        /// 判断当前产生的token是否已经过期 如果没有过期就 将token的值进行返回
        /// </summary>
        /// <returns></returns>
        public AuthorToken_App IsValidToken()
        {
            AuthorToken_App mo = null;
            string sql = "select * from authortoken_app order by ctime desc limit 1 ";
            using (IDbConnection dbConnection = dbHelper.GetDbConnection())
            {
                mo = dbConnection.QueryFirstOrDefault<AuthorToken_App>(sql);
            }
            return mo;
        }

        /// <summary>
        /// 验证token是否已经过期 如果token已经过期 返回false 否则返回true
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool IsValidToken(string token)
        {
            bool success = false;
            string sql = "select count(1) from authortoken_app where token = @token ";
            using (IDbConnection dbConnection = dbHelper.GetDbConnection())
            {
                int result = dbConnection.ExecuteScalar<int>(sql, new { token = token });
                if(result>0)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            return success;
        }
    }
}
