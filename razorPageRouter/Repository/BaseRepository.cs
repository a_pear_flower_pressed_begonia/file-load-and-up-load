﻿using razorPageRouter.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace razorPageRouter.Repository
{
    public class BaseRepository:IRepository
    {
        protected IDbHelper dbHelper { get; set; }

        public BaseRepository(IDbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public int Excute(string sql, object param)
        {
            return dbHelper.dbConnection.Execute(sql, param);
        }

        public int Excute(string sql)
        {
            return dbHelper.dbConnection.Execute(sql);
        }

        public IEnumerable<T> QueryAll<T>(string sql, object param)
        {
            return dbHelper.dbConnection.Query<T>(sql,param);
        }

        public T QueryFirst<T>(string sql, object param)
        {
            return dbHelper.dbConnection.QueryFirst<T>(sql, param);
        }

        public T Scalar<T>(string sql, object param)
        {
            return dbHelper.dbConnection.ExecuteScalar<T>(sql, param);
        }
    }
}
