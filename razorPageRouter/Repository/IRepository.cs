﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razorPageRouter.Repository
{
    public interface IRepository
    {
        T QueryFirst<T>(string sql, object param);
        IEnumerable<T> QueryAll<T>(string sql, object param);
        int Excute(string sql, object param);
        int Excute(string sql);
        T Scalar<T>(string sql, object param);
    }
}
