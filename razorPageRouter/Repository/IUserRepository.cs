﻿using razorPageRouter.Db;
using razorPageRouter.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using razorPageRouter.Models;

namespace razorPageRouter.Repository
{
    public interface IUserRepository : IRepository
    {
        bool ValidateAccout(User user);
    }

    public class UserRepository:BaseRepository, IUserRepository
    {
        public UserRepository(IDbHelper dbHelper):base(dbHelper)
        {
            dbHelper.dBType = Enum.DBType.MYSQL;
        }

        public bool ValidateAccout(User user)
        {
            bool success = false;
            string sql = "select * from users where name=@name and pwd=@pwd ";
            using (IDbConnection connection = dbHelper.GetDbConnection())
            {
                var query = connection.QueryFirstOrDefault<UserV>(sql, new {name=user.UserName,pwd=user.PassWord });
                if(query == null)
                {
                    success = false;
                }
                else
                {
                    success = true;
                }
            }
            return success;
        }
    }
}
