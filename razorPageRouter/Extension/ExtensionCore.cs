﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using razorPageRouter.Token;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using razorPageRouter.Middlerware;
using razorPageRouter.Services;
using razorPageRouter.Repository;

namespace razorPageRouter.Extension
{
    public static class ExtensionCore
    {
        /// <summary>
        /// 注入token服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddAuthorToken(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IAuthorTokenService, AuthorTokenService>();
            return services;
        }

        /// <summary>
        /// 注入自定义服务和仓储
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            //注入用户仓储和用户服务
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IUserServices, UserServices>();
            
            services.AddSingleton<ITokenRepository, TokenRepository>();
            services.AddSingleton<ITokenServices, TokenServices>();
            return services;
        }


        /// <summary>
        /// 注入自定义token验证中间件
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder AddAuthorTokenMiddleWare(this IApplicationBuilder app)
        {
            app.UseMiddleware<AuthorTokenCheckMiddlerWare>();
            return app;
        }
    }
}
