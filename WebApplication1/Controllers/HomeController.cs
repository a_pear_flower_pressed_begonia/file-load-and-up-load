﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    //如果同时配置属性路由和默认路由 属性路由会覆盖掉默认路由
    /*[Route("Home")]*/
    public class HomeController : Controller
    {
        /*[Route("Index")]*/
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}
